#Defining Car class to have position and velocity
class Car(object):
    def __init__(self, position, velocity):
        self.position = position
        self.velocity = velocity
    def move(self): #Moves the car with its current velocity
        self.position = (self.position + self.velocity)
    def change_velocity(self, new_velocity): #Change the car's velocity
        self.velocity = new_velocity

#Defining the simulation with number of cars and speed limit
def highway_list(num_cars, max_vel, max_pos):
    num_exit = 0 #Number of cars past highway
    num_entered = 0 #Total number of cars that entered the simulation
    dt = 1.0 #Time step length
    time = 0-dt #Current time. Negative so we can start at time = 0
    car_list = [] #Define array of cars
    while num_exit < num_cars:
        time += dt #Advance time
        remove_list = [] #List of cars that can be removed after they leave highway
        for i in range(len(car_list)): #Moves all the cars
            car_list[i].move()
            if car_list[i].position > max_pos: #Finds cars past highway
                remove_list.append(i)
        for num in remove_list[::-1]: #Removes cars
            car_list.pop(num)
            num_exit += 1
        if num_entered < num_cars: #Add cars to list/highway
            car = Car(0.0, max_vel)
            car_list.append(car)
            num_entered += 1
    return time

#Defining the simulation with number of cars and speed limit
def highway_dic(num_cars, max_vel, max_pos):
    num_exit = 0 #Number of cars past highway
    dt = 1.0 #Time step length
    time = 0.0-dt #Current time. Negative so we can start at time = 0
    car_dic = {} #Define dictionary of cars
    while num_exit < num_cars:
        time += dt #Advance time
        for car in car_dic: #Moves cars
            car_dic[car].move()
        num_exit = 0
        for car in car_dic: #Tacks number of cars gone from highway
            if car_dic[car].position > max_pos:
                num_exit += 1
        if len(car_dic) < num_cars: #Add cars to dictionary/highway
            car_name = 'c' + str(len(car_dic))
            car_dic[car_name] = Car(0.0, max_vel)
    return time
            
num = 1000
print highway_list(num, 1.0, 100.0)
print highway_dic(num, 1.0, 100.0)
